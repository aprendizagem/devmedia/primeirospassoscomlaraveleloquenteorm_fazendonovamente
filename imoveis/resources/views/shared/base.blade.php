<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
 
        <title>Administração de Imóveis</title>
 
        <!-- <link href="../css/bootstrap.min.css" rel="stylesheet"> -->
        <!--<script src="../js/jquery-3.2.1.min.js"></script> -->
        <!-- <script src="../js/bootstrap.min.js"></script> -->

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style>
            body { padding-top: 70px; }
        </style>
    </head>
 
    <body>
    <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">   
      <a class="navbar-brand" href="#">Imoveis</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">      
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tipo <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{route('imoveis.index', 'tipo=apartamento')}}">Apartamento</a></li>
            <li><a href="{{route('imoveis.index', 'tipo=casa')}}">Casa</a></li>
            <li><a href="{{route('imoveis.index', 'tipo=kitnet')}}">Kitnet</a></li>
           <!--  <li role="separator" class="divider"></li>
            <li><a href="#">Sobral</a></li>
            <li><a href="#">Fortaleza</a></li>  
            -->          
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-right" method="GET" action="{{route('imoveis.index', 'buscar')}}">
        <div class="form-group">
          <input name="buscar" type="text" class="form-control" placeholder="Digite o nome da cidade">
        </div>
        <button type="submit" class="btn btn-default">Pesquisar</button>   
      </form>      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
    
 <!-- 
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Início</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="{{route('imoveis.index')}}">Todos</a></li>
                        <li><a href="{{route('imoveis.index', 'tipo=apartamento')}}">Apartamentos</a></li>
                        <li><a href="{{route('imoveis.index', 'tipo=casa')}}">Casas</a></li>
                        <li><a href="{{route('imoveis.index', 'tipo=kitnet')}}">Kitnet</a></li>
                        </ul>
                </div>
            </div>
        </nav>
    -->
        <div class="container">
            @yield('content')
        </div>
</html>