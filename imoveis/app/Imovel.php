<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imovel extends Model
{
    //Mapeando meu migration para model imovel
    protected $fillable = [
        "descricao","logradouroEndereco","bairroEndereco","numeroEndereco","cepEndereco","cidadeEndereco","preco",
        "qtdQuartos","tipo","finalidade"
    ];
    protected $table = "imoveis";
}